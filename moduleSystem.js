import Table from "./tugasmodul";
const table = new Table({
  columns: ["Name", "Email", "Alamat", "Jenis Kelamin"],
  data: [
    ["Rizka Maulia", "rizkamaulia5@gmail.com", "Griya Husada Cibitung", "Wanita"],
    ["Boyke Wiliyam", "boywiliyam11@gmail.com", "Dukuh Zambrud Bekasi", "Pria"],
    ["Nabila Siti Nurjannah", "nabilasiti2@gmail.com", "Bekasi Timur Regency", "Wanita"],
    ["Rizky Ramadhan", "ikyaja01@gmail.com", "Perum Vida Bekasi", "Pria"],
    ["Caca", "cacaenakloh4@gmail.com", "Perum Grand Residence", "Wanita"]
  ]
});
const app = document.getElementById("app");
table.render(app);

  